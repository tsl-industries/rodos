# Cortex-R5F extensions
set(RODOS_DIR "${CMAKE_CURRENT_LIST_DIR}/../..")
set(board ti_rm57lx)
set(port_dir "bare-metal/ti_rm57lx")

set(is_port_baremetal TRUE)

set(RODOS_DIR "${CMAKE_CURRENT_LIST_DIR}/../..")

set(CMAKE_SYSTEM_NAME Generic)

set(CMAKE_C_COMPILER tiarmclang)
set(CMAKE_CXX_COMPILER tiarmclang)
set(CMAKE_AR tiarmar)
# set(CMAKE_ASM_FLAGS "-x ti-asm")
set(CMAKE_CXX_LINK_EXECUTABLE "<CMAKE_CXX_COMPILER> <FLAGS> <OBJECTS> -o <TARGET> <CMAKE_CXX_LINK_FLAGS> <LINK_FLAGS> <LINK_LIBRARIES>")

set(compile_and_link_options -march=armv7r -mcpu=cortex-r5 -mfloat-abi=hard -mfpu=vfpv3-d16 -mlittle-endian -marm -gdwarf-3 -Werror=ti-pragmas -Werror=ti-macros -Werror=ti-intrinsics -fno-short-wchar -fcommon)
add_compile_options(${compile_and_link_options})
add_link_options(${compile_and_link_options})
add_link_options(-Wl,--reread_libs -Wl,--diag_wrap=off -Wl,--display_error_number -Wl,--warn_sections -Wl,--rom_model -Wl,${RODOS_DIR}/src/bare-metal/${board}/halcogen/source/HL_sys_link.cmd)

set(sources_to_add
    ${RODOS_DIR}/src/bare-metal/${board}/halcogen/source/*.c
    ${RODOS_DIR}/src/bare-metal/${board}/halcogen/source/*.s
    ${RODOS_DIR}/src/bare-metal/${board}/startup/*.c
    )

set(directories_to_include
    src/bare-metal/${board}/halcogen/include
    src/bare-metal/${board}/platform-parameter/${board})

# # Not available on tiarmclang apparently
# set(libraries_to_link
#     m
# )

# set(CMAKE_VERBOSE_MAKEFILE on)