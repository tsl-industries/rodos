/**
 * @file hw_thread.cpp
 * @author Pascal Fiedler
 * @brief Hardware specific functions for scheduling/threading
 * @version 0.1
 * @date 2024-02-12
 *
 */
#include "hw_specific.h"
#include "rodos.h"
#include <stdio.h>

namespace RODOS {

extern "C" {
volatile long* contextT = nullptr;
}

constexpr long INITIAL_CPSR        = 0x01000000l;
constexpr long INITIAL_EXEC_RETURN = static_cast<long>(0xfffffffdl);

// TODO: remove unused
long* hwInitContext(long* stack, void* object) {
    stack--; // to maintain the initial stack pointer double word aligned
             // we have to decrement the stack pointer by an even number (FA-2012.02
             /* Simulate the stack frame as it would be created by a context switch
                interrupt. */

    *stack = (long)(threadStartupWrapper); // LR
    stack--;
    *stack = (long)object; // R0
    stack -= 12;           // R1 - R12,R15
    stack--;
    *stack = INITIAL_CPSR; // CPSR

    return stack;
}

void startIdleThread() {
    // printf("idle where?");
    __asm volatile(
      // " ldr r0, =0xE000ED08 	\n" /* Use the NVIC offset register to locate the stack. */
      // " ldr r0, [r0] 			\n"
      // " ldr r0, [r0] 			\n"
      // " msr msp, r0			\n" /* Set the msp back to the start of the stack. */
      " svc #0					\n" /* System call to start first task. */
      " nop					\n");
}

uintptr_t Thread::getCurrentStackAddr() {
    return reinterpret_cast<uintptr_t>(context.load());
}


extern "C" {
// __attribute__((target("arm"))) __attribute__((naked)) void SVC_Handler(void) __attribute__((weak));

// /* When the ISR is entered the following registered are saved automatically:
//  * FPSCR, S15 ... S0, xPSR, PC, LR, R12, R3, R2, R1, R0
//  * -> the other registers (R11 ... R4, S31 ... S16) must be saved and restored manually
//  * -> very helpful document: "PM0214 Programming manual - STM32F3xxx and STM32F4xxx Cortex-M4 programming manual" page 42
//  */
// __attribute__((target("arm"))) __attribute__((naked)) /* __section(".svc_handler") */ void SVC_Handler(void) {
//     // __asm volatile( // Change mode from supervisor to system to have access to the user stack pointer
//     //   "	cps #0x1F       					\n"
//     //   "	PUSH {lr}		   					\n" // save core registers on stack of interrupted thread
//     //   "	PUSH {r0-r12}					\n"
//     //   "	mrs r0, cpsr					    \n"
//     //   "	PUSH {r0}							\n"
//     //   "	bl schedulerWrapper					\n"
//     //   "										\n"
//     //   "	ldr r1, =contextT					\n" // "contextT" holds the stack pointer of next thread
//     //   "	ldr sp, [r1]						\n" // load stack pointer of next thread into stack pointer of user mode
//     //   "										\n"
//     //   "	cps #0x13							\n" // Change mode from system to supervisor to have access to the Link Register
//     //   "	ldr r1, =contextT					\n" // "contextT" holds the stack pointer of next thread
//     //   "	ldr sp, [r1]						\n" // load stack pointer of next thread into stack pointer of svc mode
//     //   "	POP {r0}							\n"
//     //   "	msr cpsr, r0						\n"
//     //   " 	POP {r0-r12}  					\n" // set mode to Supervisor
//     //   "	POP {lr}        					\n" // write r0 back to cpsr
//     //   "	bx lr								\n"
//     //   "										\n"
//     //   //   "	.align 2							\n"
//     // );
//     __asm volatile( // Change mode from supervisor to system to have access to the user stack pointer
//       "	nop                					\n"
//       "	bx lr								\n"
//       "										\n"
//       //   "	.align 2							\n"
//     );
// }
}

extern "C" {
void __asmSwitchToContext(long* context) {
    contextT = context;
}
void __asmSaveContextAndCallScheduler() {
    /* Set a SVC interrupt to request a context switch. */
    __asm volatile(
      "svc 0 \n"
      "nop					\n");
}
}

} // namespace RODOS
