/**
 * @file platform-parameter.h
 * @author Pit Hühner
 * @brief Parameters for the TI-RM57Lx platform
 * @version 0.1
 * @date 2023-12-02
 * 
 * @copyright Copyright (c) 2023
 * 
 */

#pragma once

// TODO: Complete

/** Version Number */
#define OSVERSION "ti_rm57lx"

/*************** System Configuration *********/

#undef  XMALLOC_SIZE
#define XMALLOC_SIZE		    	40*1024
#undef  DEFAULT_STACKSIZE
#define DEFAULT_STACKSIZE	        2000
#undef  SCHEDULER_STACKSIZE
#define SCHEDULER_STACKSIZE	        8
#undef  UDP_INCOMMIG_BUF_LEN
#define UDP_INCOMMIG_BUF_LEN        4 //number of "NetworkMessage (1300 Bytes each)" in FIFO