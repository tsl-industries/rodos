/**
 * @file hw_timer.cpp
 * @author Pit Hühner, Jonathan Simon
 * @brief Implementation of the Timer methods
 * @version 0.1
 * @date 2023-12-01
 *
 * @copyright Copyright (c) 2023
 *
 */

#include "rodos.h"
#include "hw_specific.h"
#include "HL_rti.h"

namespace RODOS {

int64_t Timer::microsecondsInterval = 0;


void Timer::init() {
    // rtiInit();
    // possible issue: rtiInit() resets both counter blocks
    //             --> hwInitTime() and Timer::init() in conflict

    rtiEnableNotification(rtiREG1, rtiNOTIFICATION_COUNTER1);
}

void Timer::stop() {
    rtiStopCounter(rtiREG1, rtiCOUNTER_BLOCK1);
}
void Timer::start() {
    stop();

    // reset Timer Counter Block to 0 before new start
    rtiResetCounter(rtiREG1, rtiCOUNTER_BLOCK1);
    rtiStartCounter(rtiREG1, rtiCOUNTER_BLOCK1);
}

extern "C" int64_t getOverflowedPortionOfCounter1();

int64_t Timer::getNanosecondsElapsed() {
    /* reads current value from Counter Block 1 from rtiBase Register UCx
     *  *< 0x0014,0x0034: Up Counter x Register
     *  refer to HL_reg_rti.h
     *
     *  Counter 1 should have a period of 26.67 nanoseconds
     */

    // uint64_t raw_ticks = getOverflowedPortionOfCounter0() + rtiREG1->CNT[0].FRCx;
    // uint64_t ticks     = raw_ticks + rtiREG1->CNT[0].UCx;
    // return ticks * 80 / 3; // RTI counter 0 runs at 37.5 MHz. Therefore, a t0;ick approximately equals 26.67 ns
    int64_t nanosecondsElapsed = getOverflowedPortionOfCounter1() + rtiREG1->CNT[rtiCOUNTER_BLOCK1].FRCx;
    int64_t ticks              = nanosecondsElapsed + rtiREG1->CNT[rtiCOUNTER_BLOCK1].UCx;

    return ticks * 80 / 3;
}

void Timer::setInterval(const int64_t microsecondsInterval) {
    Timer::microsecondsInterval = microsecondsInterval;
}

} // namespace RODOS