/**
 * @file hw_specific.cpp
 * @author Pit Hühner, Quang Thanh Ta <ta.quang_thanh-it21@it.dhbw-ravensburg.de>
 * @brief General hardware specific functions
 * @version 0.1
 * @date 2023-11-30
 *
 */


#include "hw_specific.h"
#include "rodos.h"
#include "HL_rti.h"
#include "HL_system.h"
#include "HL_sys_vim.h"

namespace RODOS {

void sp_partition_yield() {}

void FFLUSH() {}

void enterSleepMode() {}

void hwInitTime() {
    // Counter 1 should have a period of 0.5 microsecond
    rtiInit();
    rtiStartCounter(rtiREG1, rtiCOUNTER_BLOCK0);
    rtiEnableNotification(rtiREG1, rtiNOTIFICATION_COUNTER0);
    hwEnableInterrupts();
}

// int getCurrentIRQ(){ return 0; }

// Defined inside HL_notification.c
extern "C" int64_t getOverflowedPortionOfCounter0();

int64_t hwGetNanoseconds() {
    int64_t raw_ticks = getOverflowedPortionOfCounter0() + rtiREG1->CNT[0].FRCx;
    int64_t ticks     = raw_ticks + rtiREG1->CNT[0].UCx;
    return ticks * 80 / 3; // RTI counter 0 runs at 37.5 MHz. Therefore, a tick approximately equals 26.67 ns
}

void hwInit() {
    // Already done in `halcogen/source/HL_sys_startup.c`
}

void hwEnableInterrupts() {
    _enable_IRQ_interrupt_();
}

} // namespace RODOS
