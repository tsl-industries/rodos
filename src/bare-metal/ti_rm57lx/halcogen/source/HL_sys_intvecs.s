/*-------------------------------------------------------------------------------
 HL_sys_intvecs.s

 Copyright (C) 2009-2018 Texas Instruments Incorporated - www.ti.com


  Redistribution and use in source and binary forms, with or without
  modification, are permitted provided that the following conditions
  are met:

    Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.

    Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the
    distribution.

    Neither the name of Texas Instruments Incorporated nor the names of
    its contributors may be used to endorse or promote products derived
    from this software without specific prior written permission.

  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

----------------------------------------------------------------------------*/

    .syntax unified
    .cpu cortex-r5
    .arm

    .section .intvecs,"a",%progbits
    .global _c_int00
    .type    _c_int00, %function

/*-------------------------------------------------------------------------------*/
@ import reference for interrupt routines

    .extern c_int00
    .extern phantomInterrupt
    .extern SVC_Handler
    .weak resetEntry

/*-------------------------------------------------------------------------------*/
@ interrupt vectors

_c_int00:
        b   c_int00
undefEntry:
        b   undefEntry
svcEntry:
        b   ASM_SVC_Handler
prefetchEntry:
        b   prefetchEntry
dataEntry:
        b   dataEntry
        b   phantomInterrupt
        ldr pc,[pc,#-0x1b0]
        ldr pc,[pc,#-0x1b0]

/*-------------------------------------------------------------------------------*/

    .text
    .global ASM_SVC_Handler
    .type   ASM_SVC_Handler, %function
    .extern contextT

ASM_SVC_Handler:
    cps #0x1F
    PUSH {lr}                           // save core registers on stack of interrupted thread
    PUSH {r0-r12}
    mrs r0, cpsr
    PUSH {r0}
    bl schedulerWrapper
    
    ldr r1, =contextT                   // "contextT" holds the stack pointer of next thread
    ldr sp, [r1]                        // load stack pointer of next thread into stack pointer of user mode

    cps #0x13                           // Change mode from system to supervisor to have access to the Link Register
    ldr r1, =contextT                   // "contextT" holds the stack pointer of next thread
    ldr sp, [r1]                        // load stack pointer of next thread into stack pointer of svc mode
    POP {r0}
    msr cpsr, r0
    POP {r0-r12}                    // set mode to Supervisor
    POP {lr}                            // write r0 back to cpsr
    bx lr
    nop
    bx lr