/** @file HL_notification.c 
*   @brief User Notification Definition File
*   @date 11-Dec-2018
*   @version 04.07.01
*
*   This file  defines  empty  notification  routines to avoid
*   linker errors, Driver expects user to define the notification. 
*   The user needs to either remove this file and use their custom 
*   notification function or place their code sequence in this file 
*   between the provided USER CODE BEGIN and USER CODE END.
*
*/

/* 
* Copyright (C) 2009-2018 Texas Instruments Incorporated - www.ti.com  
* 
* 
*  Redistribution and use in source and binary forms, with or without 
*  modification, are permitted provided that the following conditions 
*  are met:
*
*    Redistributions of source code must retain the above copyright 
*    notice, this list of conditions and the following disclaimer.
*
*    Redistributions in binary form must reproduce the above copyright
*    notice, this list of conditions and the following disclaimer in the 
*    documentation and/or other materials provided with the   
*    distribution.
*
*    Neither the name of Texas Instruments Incorporated nor the names of
*    its contributors may be used to endorse or promote products derived
*    from this software without specific prior written permission.
*
*  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS 
*  "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT 
*  LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
*  A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT 
*  OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, 
*  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT 
*  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
*  DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
*  THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT 
*  (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE 
*  OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
*
*/


/* Include Files */

#include "HL_esm.h"
#include "HL_can.h"
#include "HL_sci.h"
#include "HL_spi.h"
#include "HL_rti.h"
#include "HL_i2c.h"
#include "HL_epc.h"
#include "HL_sys_dma.h"

/* USER CODE BEGIN (0) */
/* USER CODE END */
//#pragma WEAK(esmGroup1Notification) - legacy pragma, see HL_esm.h
/*parameters are unused, otherwise delete "__attribute__((unused))""*/
__attribute__((interrupt)) void esmGroup1Notification(__attribute__((unused)) esmBASE_t *esm, __attribute__((unused)) uint32 channel)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (1) */
/* USER CODE END */
}

/* USER CODE BEGIN (2) */
/* USER CODE END */
//#pragma WEAK(esmGroup2Notification) - legacy pragma, see HL_esm.h
/*parameters are unused, otherwise delete "__attribute__((unused))""*/ 
__attribute__((interrupt)) void esmGroup2Notification(__attribute__((unused)) esmBASE_t *esm, __attribute__((unused)) uint32 channel)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (3) */
/* USER CODE END */
}

/* USER CODE BEGIN (4) */
/* USER CODE END */
//#pragma WEAK(esmGroup3Notification) - legacy pragma, see HL_esm.h
/*parameters are unused, otherwise delete "__attribute__((unused))""*/
__attribute__((interrupt)) void esmGroup3Notification(__attribute__((unused)) esmBASE_t *esm, __attribute__((unused)) uint32 channel)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (5) */
/* USER CODE END */
    for(;;)
    { 
    }/* Wait */  
/* USER CODE BEGIN (6) */
/* USER CODE END */
}

/* USER CODE BEGIN (7) */
/* USER CODE END */

//#pragma WEAK(dmaGroupANotification) - legacy pragma, see HL_sys_dma.h
/*parameters are unused, otherwise delete "__attribute__((unused))""*/
void dmaGroupANotification(__attribute__((unused)) dmaInterrupt_t inttype, __attribute__((unused)) uint32 channel)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (8) */
/* USER CODE END */
}

/* USER CODE BEGIN (9) */
/* USER CODE END */

/* USER CODE BEGIN (10) */
/* USER CODE END */

/* USER CODE BEGIN (11) */
int64_t counter0Overflows = 0;
int64_t getOverflowedPortionOfCounter0(void) {
    return counter0Overflows << 32;
}

int64_t counter1Overflows = 0;
int64_t getOverflowedPortionOfCounter1(void) {
    return counter1Overflows << 32;
}

/* USER CODE END */

/*parameters are unused, otherwise delete "__attribute__((unused))""*/
void rtiNotification(__attribute__((unused)) rtiBASE_t* rtiREG, __attribute__((unused)) uint32 notification) {
    /*  enter user code between the USER CODE BEGIN and USER CODE END. */
    /* USER CODE BEGIN (12) */
    if(notification == rtiNOTIFICATION_COUNTER0) {
        ++counter0Overflows;
    } else if (notification == rtiNOTIFICATION_COUNTER1) {
        ++counter1Overflows;
    } else {
        // Should never happen!! TODO: Check for this
    }

    /* USER CODE END */
}

/* USER CODE BEGIN (13) */
/* USER CODE END */
//#pragma WEAK(canErrorNotification) - legacy pragma, see HL_can.h
/*parameters are unused, otherwise delete "__attribute__((unused))""*/
void canErrorNotification(__attribute__((unused)) canBASE_t *node, __attribute__((unused)) uint32 notification)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (16) */
/* USER CODE END */
}

//#pragma WEAK(canStatusChangeNotification) - legacy pragma, see HL_can.h
/*parameters are unused, otherwise delete "__attribute__((unused))""*/
void canStatusChangeNotification(__attribute__((unused)) canBASE_t *node, __attribute__((unused)) uint32 notification)  
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (17) */
/* USER CODE END */
}

//#pragma WEAK(canMessageNotification) - legacy pragma, see HL_can.h
/*parameters are unused, otherwise delete "__attribute__((unused))""*/
void canMessageNotification(__attribute__((unused)) canBASE_t *node, __attribute__((unused)) uint32 messageBox)  
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (18) */
/* USER CODE END */
}

/* USER CODE BEGIN (19) */
/* USER CODE END */
//#pragma WEAK(i2cNotification) - legacy pragma, see HL_i2c.h
/*parameters are unused, otherwise delete "__attribute__((unused))""*/
void i2cNotification(__attribute__((unused)) i2cBASE_t *i2c, __attribute__((unused)) uint32 flags)      
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (24) */
/* USER CODE END */
}

/* USER CODE BEGIN (25) */
/* USER CODE END */

//#pragma WEAK(sciNotification) - legacy pragma, see HL_sci.h
/*parameters are unused, otherwise delete "__attribute__((unused))""*/
void sciNotification(__attribute__((unused)) sciBASE_t *sci, __attribute__((unused)) uint32 flags)     
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (32) */
/* USER CODE END */
}

/* USER CODE BEGIN (33) */
/* USER CODE END */
//#pragma WEAK(spiNotification) - legacy pragma, see HL_spi.h
/*parameters are unused, otherwise delete "__attribute__((unused))""*/
void spiNotification(__attribute__((unused)) spiBASE_t *spi, __attribute__((unused)) uint32 flags)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (34) */
/* USER CODE END */
}

/* USER CODE BEGIN (35) */
/* USER CODE END */
//#pragma WEAK(spiEndNotification) - legacy pragma, see HL_spi.h
/*parameters are unused, otherwise delete "__attribute__((unused))""*/
void spiEndNotification(__attribute__((unused)) spiBASE_t *spi)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (36) */
/* USER CODE END */
}

/* USER CODE BEGIN (37) */
/* USER CODE END */



/* USER CODE BEGIN (46) */
/* USER CODE END */


/* USER CODE BEGIN (50) */
/* USER CODE END */


/* USER CODE BEGIN (53) */
/* USER CODE END */


/* USER CODE BEGIN (56) */
/* USER CODE END */

//#pragma WEAK(epcCAMFullNotification) - legacy pragma, see HL_epc.h
void epcCAMFullNotification(void)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (57) */
/* USER CODE END */
}
//#pragma WEAK(epcFIFOFullNotification) - legacy pragma, see HL_epc.h
/*parameters are unused, otherwise delete "__attribute__((unused))""*/
void epcFIFOFullNotification(__attribute__((unused)) uint32 epcFIFOStatus)
{
/*  enter user code between the USER CODE BEGIN and USER CODE END. */
/* USER CODE BEGIN (58) */
/* USER CODE END */
}

/* USER CODE BEGIN (59) */
/* USER CODE END */


/* USER CODE BEGIN (61) */
/* USER CODE END */

/* USER CODE BEGIN (63) */
/* USER CODE END */
