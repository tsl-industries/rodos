export TARGET_LIB=ti_rm57lx
export ARCH=ti_rm57lx
export LINKER_SCRIPT="${RODOS_SRC}/bare-metal/${ARCH}/halcogen/source/HL_sys_link.cmd"

RODOS_ARCH_SRC1="${RODOS_SRC}/bare-metal/${ARCH}"
RODOS_ARCH_SRC2="${RODOS_SRC}/bare-metal/${ARCH}/halcogen/include"
RODOS_ARCH_SRC3="${RODOS_SRC}/bare-metal/${ARCH}/halcogen/source"
RODOS_ARCH_SRC4="${RODOS_SRC}/bare-metal/${ARCH}/platform-parameter/${ARCH}"

SRCS[1]="${RODOS_SRC}/bare-metal-generic"
SRCS[2]="${RODOS_SRC}/bare-metal/${ARCH}"
SRCS[3]="${RODOS_SRC}/bare-metal/${ARCH}/halcogen/source"

export INCLUDES=${INCLUDES}" -I ${RODOS_SRC}/bare-metal/${ARCH} \
    -I ${RODOS_ARCH_SRC2} \
    -I ${RODOS_ARCH_SRC4} "

export INCLUDES_TO_BUILD_LIB=" -I ${RODOS_SRC}/bare-metal-generic  \
    -I ${RODOS_ARCH_SRC1} \
    -I ${RODOS_ARCH_SRC2} \
    -I ${RODOS_ARCH_SRC3} \
    -I ${RODOS_ARCH_SRC4} "

export LINKFLAGS=" -Wl,--rom_model,${LINKER_SCRIPT},-l${RODOS_LIBS}/${TARGET_LIB}/librodos.a"
export CFLAGS=${CFLAGS}" -mcpu=cortex-r5 -mfpu=vfpv3-d16 -mfloat-abi=hard"
export CPPFLAGS=${CPPFLAGS}" -Wno-register"

export CPP_COMP="${CXX:-tiarmclang} "
export C_COMP="${CC:-tiarmclang} "
export AR="${AR:-tiarmar} "
